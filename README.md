Secucad
=======

Secucad is a Security Camera Downloader. It is meant to periodically download images from a security camera and save them on local disk. I keeps them for a configurable amount of time. It is written in python and uses pyforms for the GUI. It has a windows service using the pywin32 library.


Generating the service with pyinstaller:
```
pyinstaller --add-data "secucad.ini;." --hidden-import win32timezone downloadservice.py
```

Install service and start service
```
downloadservice.exe install
net start SecucadDownloadService
