import os
import sys
import time
import requests
from configparser import ConfigParser
from datetime import datetime
from pathlib import Path
import signal
import logging
import mimetypes


def signal_handler(signal, frame):
        print('Shutting down')
        d.stop = True
        sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)
log = logging.getLogger(__name__)


class Downloader(object):
    def __init__(self, configfile='secucad.ini'):
        self.config = ConfigParser()
        self.config.read(configfile)
        self.frequency = self.config.getfloat('main', 'frequency', fallback=1)
        home = str(Path.home())
        self.destination = self.config.get('main', 'destination',
                                           fallback=home)
        self.keep = self.config.getint('main', 'keep', fallback=4)
        self.url = self.config.get('main', 'url', fallback='http://')
        self.stop = False

    def download(self):
        log.debug("Getting url {}".format(self.url))
        try:
            resp = requests.get(self.url)
        except Exception as e:
            log.error(str(e))
            raise
        now = datetime.now().strftime('%Y-%m-%d_%H_%M_%S')
        mime = resp.headers.get('Content-Type')
        ext = mimetypes.guess_extension(mime)
        fname = os.path.join(self.destination, "{}{}".format(now, ext))
        log.debug('Saving file as {}'.format(fname))
        with open(fname, 'wb') as f:
            f.write(resp.content)

    def main(self):
        while not self.stop:
            self.download()
            sleepytime = self.frequency*60
            print("Sleeping {} seconds".format(sleepytime))
            time.sleep(sleepytime)


if __name__ == '__main__':
    print("Starting Secucad with arguments {}" .format(sys.argv))
    if len(sys.argv) == 1 or sys.argv[1] == 'start':
        print("Starting daemon")
        d = Downloader()
        d.main()

    elif sys.argv[1] == 'download':
        print("Downloading single file")
        d = Downloader()
        d.download()
