import win32api
import win32serviceutil
import win32service
import win32event
import servicemanager
import socket
import sys
import os
from downloader import Downloader
import logging

_here = "C:\Program Files\Secucad"

logging.basicConfig(
    filename=os.path.join(_here, 'secucad.log'),
    level=logging.DEBUG,
    format='%(asctime)s-%(levelname)s-%(message)s'
)

pylog = logging.getLogger()


class DownloadService(win32serviceutil.ServiceFramework):
    _svc_name_ = 'SecucadDownloadService'
    _svc_display_name_ = 'SecucadDownloadService'

    def __init__(self, args):
        pylog.info("Initializing Seucucad Download Service")
        pylog.debug("Starting")
        pylog.debug("pwd: {}".format(os.getcwd()))
        pylog.debug("Here: {}".format(_here))
        win32serviceutil.ServiceFramework.__init__(self, args)
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)
        socket.setdefaulttimeout(60)
        self.initService()

    def SvcStop(self):
        self.log.info("Stopping")
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)

    def initService(self):
        self.log = logging.getLogger(__name__)
        self.log.info("Initializing Secucad Download Service")
        cfname = os.path.join(_here, 'secucad.ini')
        self.log.info('Loading config file {}'.format(cfname))
        self.downloader = Downloader(cfname)

    def SvcDoRun(self):
        servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
                              servicemanager.PYS_SERVICE_STARTED,
                              (self._svc_name_, ''))
        try:
            self.main()
        except Exception as e:
            self.log.error(str(e))
            self.SvcStop()

    def main(self):
        self.log.debug("Starting main loop")
        rc = None
        while rc != win32event.WAIT_OBJECT_0:
            self.log.debug("rc: {}".format(rc))
            self.log.debug("Downloading {}".format(self.downloader.url))
            try:
                self.downloader.download()
            except Exception as e:
                self.log.error("An error occured")
                self.log.error(str(e))
                servicemanager.LogErrorMsg(str(e))
            self.log.debug("Finished downloading")
            sleepytime = self.downloader.frequency*60*1000
            self.log.debug("wating for {} milliseconds".format(sleepytime))
            rc = win32event.WaitForSingleObject(self.hWaitStop, sleepytime)
            self.log.debug("rc==win32event.WAIT_OBJECT_0? {}".format(
                rc == win32event.WAIT_OBJECT_0))
        self.log.debug("Exited loop")


if __name__ == '__main__':
    if len(sys.argv) == 1:
        servicemanager.Initialize()
        servicemanager.PrepareToHostSingle(DownloadService)
        servicemanager.StartServiceCtrlDispatcher()
    else:
        win32serviceutil.HandleCommandLine(DownloadService)
