from pyforms.utils.settings_manager import conf
import settings
conf += settings

import pyforms
from pyforms import BaseWidget
from pyforms.controls import ControlText, ControlDir, ControlButton
from configparser import ConfigParser
import os
# from cameraservice import CameraService
import servicemanager
from pathlib import Path


curdir = os.path.abspath(os.path.dirname(__file__))


DESC = """Secucad allows you to periodically download files from a url and save them in a folder on the current PC.
It's intended use-case is to download images from a security camera and save those on the local computer.
By choosing a folder that is synchronized with cloud services like Dropbox, you can also save the images in the cloud."""


class Secucad(BaseWidget):

    def __init__(self):
        super(Secucad, self).__init__('Secucad')

        # Definition of the forms fields

        home = str(Path.home())
        self.config_filename = os.path.join(curdir, 'secucad.ini')
        self.config = ConfigParser({'frequency': '1', 'destination': home,
                                    'keep': '4', 'url': 'http://'})
        self.config.add_section('main')

        if os.path.exists(os.path.join(curdir, 'secucad.ini')):
            self.config.read(self.config_filename)

        self.frequency = ControlText('Number of minutes per image:',
                                     default=self.config.get('main',
                                                             'frequency'))
        self.destination = ControlDir('Folder to save the images to:',
                                      default=self.config.get('main',
                                                              'destination'))
        self.keep = ControlText('Number of weeks to keep images:',
                                default=self.config.get('main', 'keep'))
        self.url = ControlText('Address (url) to download images from:',
                               default=self.config.get('main', 'url'))
        self._save = ControlButton('Save')
        self._start = ControlButton('Start')
        self.formset = [DESC, 'url', 'destination', 'frequency', 'keep',
                        ('_save', '_start')]

        self._save.value = self.save

    def save(self):
        self.config.set('main', 'frequency', str(self.frequency.value))
        self.config.set('main', 'destination', str(self.destination.value))
        self.config.set('main', 'keep', str(self.keep.value))
        self.config.set('main', 'url', str(self.url.value))
        self.config.write(open(self.config_filename, 'w'))

    def startService(self):
        servicemanager.Initialize()
        # servicemanager.PrepareToHostSingle(CameraService)
        servicemanager.StartServiceCtrlDispatcher()


# Execute the application
if __name__ == "__main__":
    pyforms.start_app(Secucad, geometry=(150, 150, 800, 200))
