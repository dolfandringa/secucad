import os

SETTINGS_PRIORITY = 1
PYFORMS_STYLESHEET = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                  'style.css')
