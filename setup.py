from setuptools import setup, find_packages
# To use a consistent encoding
from os import path
import codecs
import wtforms_jsonschema2

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with codecs.open('README.rst') as f:
    long_description = f.read()


extras = {
    'test': ['pytest', 'pytest-cov']
}

setup(
    name="secucad",
    version=wtforms_jsonschema2.__version__,
    description="Secucad is a downloader for pictures from IP based security cams.",
    long_description=long_description,
    url="https://github.com/dolfandringa/secucad",
    author="Dolf Andringa",
    author_email="dolfandringa@gmail.com",
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    install_requires=['pyforms','requests'],
    setup_requires=['pytest-runner'],
    tests_require=extras['test'],
    extras_require=extras,
    project_urls={
        'Source': 'https://github.com/dolfandringa/secucad/'
    },
    classifiers=[
        'Programming Language :: Python :: 3',
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License'
    ],
    keywords='camera desktop image downloader'
)
